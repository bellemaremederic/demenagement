-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 05 Mai 2016 à 17:05
-- Version du serveur :  5.6.20-log
-- Version de PHP :  7.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `demenagement`
--

-- --------------------------------------------------------

--
-- Structure de la table `boite`
--

DROP TABLE IF EXISTS `boite`;
CREATE TABLE IF NOT EXISTS `boite` (
  `ID` varchar(20) NOT NULL,
  `Salle` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `boite`
--

INSERT INTO `boite` (`ID`, `Salle`) VALUES
('Salon001', 'Salon');

-- --------------------------------------------------------

--
-- Structure de la table `objet`
--

DROP TABLE IF EXISTS `objet`;
CREATE TABLE IF NOT EXISTS `objet` (
`ID` int(11) NOT NULL,
  `Nom` text NOT NULL,
  `Qty` int(11) NOT NULL,
  `Boite` varchar(20) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `objet`
--

INSERT INTO `objet` (`ID`, `Nom`, `Qty`, `Boite`) VALUES
(1, 'Table', 1, 'Salon001'),
(3, 'Lampe', 1, 'Salon001');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `boite`
--
ALTER TABLE `boite`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `objet`
--
ALTER TABLE `objet`
 ADD PRIMARY KEY (`ID`), ADD KEY `Boite` (`Boite`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `objet`
--
ALTER TABLE `objet`
MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `objet`
--
ALTER TABLE `objet`
ADD CONSTRAINT `Boite_ck_boite` FOREIGN KEY (`Boite`) REFERENCES `boite` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
